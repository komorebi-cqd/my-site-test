import { defineConfig } from 'unocss'
import presetWind from '@unocss/preset-wind'

export default defineConfig({
  presets: [
    presetWind({
    }),
  ],
  rules: [
    ["d-color", { color: 'var(--color-default)' }],
    ["d-color-dark", { color: 'var(--color-dark-default)' }],
    ["font-candyshop", { "font-family": "candyshop"}]
  ]
})