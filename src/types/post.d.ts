//index.d.ts

declare namespace Post {
  type Content = {
    tags: string[];
    title: string;
    summary: string;
    external_urls?: string;
  };

  type State = {
    likes: number;
    viewDetailCount: number;
    comments: number;
  }

  type Item = {
    id: number;
    createdAt: string;
    metadata: {
      uri: string;
      content: Content;
    };
    state: State;
  };
}

