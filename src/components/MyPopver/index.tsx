import React, { useState,useRef } from "react";
import { Popover, Transition } from "@headlessui/react";
import { usePopper } from 'react-popper'

type Props = {
  children: React.ReactNode;
  placement: "top" | "left" | "right" | "bottom";
};

const index: React.FC<Props> = ({ children,placement = "top" }) => {
  const [open, setOpen] = useState(false);
  let referenceElement = useRef(null)
  let popperElement = useRef(null)
  let { styles, attributes } = usePopper(referenceElement as unknown as Element, popperElement as unknown as HTMLElement, {placement: placement, strategy: "absolute"})

  const moveEnter = () => {
    setOpen(true);
  };

  const moveLeave = () => {
    setOpen(false);
  };

  return (
    <Popover as="div" className="relative" onMouseEnter={() => moveEnter()}
    onMouseLeave={() => moveLeave()}>
      <Popover.Button
        as="div"
        ref={referenceElement}
        className="inline-flex"
      >
        {children}
      </Popover.Button>
      {/* {
        open && <Popover.Panel ref={referenceElement} style={{...styles.popper, top: "-38px"}}
        {...attributes.popper} static className={` bg-zinc-600 text-white rounded-lg shadow-lg px-3 py-1 whitespace-nowrap`}>cqd</Popover.Panel>
      } */}
      <Transition
        show={open}
        enter="transition ease-out duration-200"
        enterFrom="opacity-0 translate-y-6"
        enterTo="opacity-100 translate-y-0"
        leave="transition ease-in duration-150"
        leaveFrom="opacity-100 translate-y-0"
        leaveTo="opacity-0 translate-y-6"
      >
        <Popover.Panel ref={referenceElement} style={{...styles.popper, top: "-38px"}}
        {...attributes.popper} static className={` bg-zinc-600 text-white rounded-lg shadow-lg px-3 py-1 whitespace-nowrap`}>cqd</Popover.Panel>
      </Transition>
    </Popover>
  );
};

export default index;
