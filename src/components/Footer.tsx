import React from 'react'
import FooterImg from "@/assets/img/footer.png";

function Footer() {
    return (
        <div>
            <div className=' aspect-[125/46] max-w-screen-lg w-full mx-auto px-5 box-border sm:-mb-[3.4rem]  lg:-mb-[5.5rem] md:-mb-[5.2rem] -mb-[2.5rem] mt-10'>
                <img src={FooterImg} alt="" className='footer-img max-w-screen-lg object-contain ' />
            </div>
            <footer className='w-full text-zinc-500  border-t'>
                <div className='max-w-screen-lg mx-auto py-7'>
                    ©版权所有
                </div>
            </footer>
        </div>
    )
}

export default Footer
