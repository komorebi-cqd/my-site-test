import React from 'react'

const index: React.FC<{ post: Post.Item }> = ({ post }) => {
    return (
        <a className='diy-post cursor-pointer rounded-2xl overflow-hidden flex flex-col items-center group relative border sm:hover:bg-hover transition-all hover:opacity-100' >
            <div className='w-full flex-1 aspect-video'>
                <img className=' object-cover hover:scale-110 transition-all' src={post.metadata.uri} alt="" />
            </div>
            <div className='px-3 py-2 w-full min-w-0 flex flex-col text-sm space-y-2 sm:px-5 sm:py-4 h-auto sm:h-[163px] bg-white z-1'>
                <div className=' flex-1  space-y-2 line-clamp-3 h-[75px]'>
                    <div className='font-bold text-zinc-700 text-base break-all'>{post.metadata.content.title}</div>
                    <div className='text-zinc-500 line-clamp-3 break-all'>{post.metadata.content.summary}</div>
                </div>
                <div className='xlog-post-meta text-zinc-400 flex items-center text-[13px] truncate space-x-2'>
                    <span>1111</span>
                    <span>2222</span>
                    <span>333</span>
                </div>
                <div className='flex items-center space-x-1 text-xs sm:text-sm overflow-hidden'>
                    <div className='whitespace-nowrap text-zinc-400 hidden sm:inline-block'>
                        1个月前
                    </div>
                </div>
            </div>
        </a>
    )
}

export default index