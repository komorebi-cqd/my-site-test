
import { Link, useLocation } from "react-router-dom";
// import DogoSvg from "@/assets/svg/dogo.svg";
// import ThemeSwitcher from '@/components/ThemeSwitcher'
import MyPopover from "./MyPopver"
import HeadBanner from "@/assets/img/head_banner.png";
import Avater from "@/assets/img/avater.png";

const navList = [
    { id: 1, text: "主页", link: "/" },
    { id: 2, text: "归档", link: "/archives" },
    { id: 3, text: "挑战", link: "/challenge" },
    { id: 4, text: "关于", link: "/about" },
]

function Header() {

    const { pathname } = useLocation();

    return (
        <header className="header-container border-b border-solid relative">
            <div className=" absolute top-0 left-0 right-0 bottom-0 overflow-hidden -z-10">
                <img src={HeadBanner} alt="" draggable="false" className=" object-cover  w-auto mx-auto h-full select-none" />
            </div>
            <div className="px-5 max-w-screen-lg h-full flex flex-col items-center mx-auto">
                {/* 标语  以后可能有用 */}
                <div className=" mb-auto"></div>
                <div className="flex py-12 w-full hover:scale-[103%] transition-all ease-linear duration-200">
                    <div className="flex space-x-6 sm:space-x-8 w-full bg-white bg-opacity-50 backdrop-blur-sm rounded-3xl p-4 pb-[52px] sm:p-8 sm:pb-8 z-[1] border border-white">
                        <span className="max-w-[100px] max-h-[100px] sm:max-w-[150px] sm:max-h-[150px] rounded-full overflow-hidden">
                            <img className="w-full h-full" src={Avater} alt="" />
                        </span>
                        <div className="flex-1 min-w-0 relative space-y-2 sm:space-y-3 min-h-[108px]">
                            <div className="font-candyshop text-3xl sm:text-4xl font-bold leading-snug break-words min-w-0 text-[--theme-color]" >Hi, CQD</div>
                            <div className="site-description leading-snug text-sm sm:text-base line-clamp-4 whitespace-pre-wrap ">为自己而活，是人一生中最重要的觉醒</div>
                            <div className=" flex gap-x-5 text-zinc-500 space-x-0 sm:space-x-3 flex-col sm:flex-row text-sm sm:text-base">
                                <span className="block">
                                    <button className="rounded-3xl px-3 transition-all  hover:bg-zinc-100 dark:hover:bg-zinc-500">
                                        <span className="font-medium text-zinc-700 mr-2">{0}</span>篇博客
                                    </button>
                                </span>
                                <span className="block">
                                    <Link to="/challenge" className="rounded-3xl px-3 block transition-all  hover:bg-zinc-100 dark:hover:bg-zinc-500">
                                        <span className="font-medium text-zinc-700 mr-2">{0}</span>个挑战
                                    </Link>
                                </span>
                            </div>
                            <div>
                                <MyPopover>
                                    ccc
                                </MyPopover>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="text-gray-500 flex items-center justify-between w-full mt-auto">
                    <div className="flex overflow-x-auto scrollbar-hide space-x-5 border-b border-none mb-0">
                        {navList.map(nav =>
                            <Link
                                className={`inline-flex items-center h-10 whitespace-nowrap cursor-pointer transition-colors relative after:absolute after:content-['']  after:h-[2px] after:transition-all after:bottom-0 text-gray-500 hover:text-gray-700 hover:after:left-0 hover:after:right-0  ${(pathname === "/" && nav.link === "/") || (pathname !== "/" && nav.link !== "/" && pathname.startsWith(nav.link)) ? "text-gray-700 after:left-0 after:right-0 after:bg-[--theme-color]" : "after:left-1/2 after:right-1/2 after:bg-gray-700"}`}
                                to={nav.link}
                                key={nav.id}>
                                {nav.text}
                            </Link>)}
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;
