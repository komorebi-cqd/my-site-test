import React from 'react'
import { useParams } from "react-router-dom";

const ChallengeMark = () => {

    const { mark } = useParams();

    return (
        <div>ChallengeMark: {mark}</div>
    )
}

export default ChallengeMark;