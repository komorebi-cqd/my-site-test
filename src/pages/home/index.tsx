import React, { useState } from 'react'
import DiyPost from "@/components/DiyPost";


const defaultPostList = [
    {
        id: 1,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 2,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "在博客融入一个跨平台作品集",
                summary:
                    "长久以来# 我一直将个人博客视为一个理想的展示个人 IP 的 “个人网站”，而不仅仅是发布文章的平台。我曾在 2014 年初学编程时使用 WordPress 建站 《世界，你好！》；入了前端坑后，在 2017 年我转向了 Hexo 《做了一点微小的改动》；Web3 飞升后…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 3,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 4,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 5,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 6,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    },
    {
        id: 7,
        createdAt: "2023-08-17T22:01:54.000Z",//创建时间
        metadata: {
            uri: "https://img1.baidu.com/it/u=2838155405,2417185975&fm=253&fmt=auto&app=138&f=JPG?w=889&h=500", //封面
            content: {
                tags: ["post", "创作集"],
                title: "优雅使用 Cloudflare WARP 应对  RSSHub 反爬难题",
                summary:
                    "🕊️ 本文送给更开放的互联网 起因是看到 @geekbb 介绍 Warp 的推文。尽管 Warp 已经发布了很长时间，就保护 IP 隐私而言，它并没有 iCloud Private Relay 好用，我也没有魔法上网的需求。但是我突然意识到，我还是有隐藏 IP 的需求。\n\n在开…",
                external_urls: "", //外链

            }
        },
        state: {
            likes: 10, //喜欢人数
            viewDetailCount: 100, //浏览人数
            comments: 1, //评论数量
        }
    }
]

const Home = () => {
    const [postList] = useState(defaultPostList)

    return (
        <div className='d-color dark:d-color-dark'>
            <div className='flex overflow-x-auto space-x-3 text-sm mb-6'></div>
            <div className='grid md:grid-cols-3 gap-3 sm:grid-cols-1'>
                {
                    postList.map(post => {
                        return <DiyPost post={post} key={post.id} />
                    })
                }
            </div>
        </div>
    )
}

export default Home;
