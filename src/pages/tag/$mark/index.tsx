import React from 'react'
import { useParams } from "react-router-dom";

const TagMark = () => {

    const { mark } = useParams();

    return (
        <div>TagMark: {mark}</div>
    )
}

export default TagMark;