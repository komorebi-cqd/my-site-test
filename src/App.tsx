
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import routes from './routes';

const router = createBrowserRouter(routes)

function App() {
  return (
    <RouterProvider router={router} fallbackElement={<p>加载中......</p>} />
  )
}

export default App