import { RouteObject } from "react-router-dom";
import Layout from '@/components/Layout';
import Home from "@/pages/home";
import TagMark from "@/pages/tag/$mark";
import Archives from "@/pages/archives";  //档案  总览
import Challenge from "@/pages/challenge"; //挑战 总览
import ChallengeMark from "@/pages/challenge/$mark";  //挑战详情
import About from "@/pages/about"; //关于我
import NotFount from "@/pages/404";

const routes: RouteObject[] = [
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                index: true,
                element: <Home />
            }, {
                path: 'archives',
                element: <Archives />
            }, {
                path: "tag/:mark",
                element: <TagMark />
            }, {
                path: "challenge", //挑战
                element: <Challenge />
            }, {
                path: "challenge/:mark", //挑战详情
                element: <ChallengeMark />
            }, {
                path: "about",
                element: <About />
            },
            { path: "*", element: <NotFount /> },
        ]
    }
];


export default routes;